package com.example;
import java.util.Scanner;
/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
     private String t;
     public boolean verificaPalindromo(String palindromo) {
         String[] arr = palindromo.split("");

         for(int i = 1; i<=arr.length;i++){
            if(!arr[i-1].equals(arr[arr.length-i])){
                return false;
            } else {
                return true;
            }
         }
         return false;
     }
 
}