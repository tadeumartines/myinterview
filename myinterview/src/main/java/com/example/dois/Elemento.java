package com.example.dois;

public class Elemento {
    public Elemento proxElemento;
    public String valor;

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Elemento getProxElemento() {
        return proxElemento;
    }

    public void setProxElemento(Elemento proxElemento) {
        this.proxElemento = proxElemento;
    }
    public Elemento(String newvalue) {
        this.valor = newvalue;
    }}