package com.example;

public class Application {
    public static void main(String[] args) {
        TASK1 task1 = new TASK1();
        TASK2 task2 = new TASK2();
        TASK3 task3 = new TASK3();

        System.out.println("Task 1");

        if(task1.verificaPalindromo("subi noonibus")){
            System.out.println("É Palindromo");
        } else {
            System.out.println("Não é Palindromo");
        }

        System.out.println("\n\nTask 2");

        TASK2 lista = new TASK2();
        lista.addlist("tadeu");
        lista.addlist("teste");
        lista.addlist("ameba");
        lista.addlist("vai");

        lista.imprime();

        System.out.println(lista.getTam());

        lista.removelist("teste");

        System.out.println(lista.getTam());

        lista.imprime();

        System.out.println("\n\nTask 3");

        task3.preenchelista();





    }




}

