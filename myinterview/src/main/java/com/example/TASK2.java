package com.example;


import com.example.dois.Elemento;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 */


public class TASK2 {
    public Elemento primelement;
    public Elemento ultelement;
    public int tam;

    public Elemento pegaposicao(int posicao){
        Elemento teste = this.primelement;
        for (int i=0;i<posicao;i++) {
            if(teste.getProxElemento()!=null) {
                teste = teste.getProxElemento();
            }
        }
        return teste;
    }

    public void addlist(String newvalue){
        Elemento teste = new Elemento(newvalue);
        if(this.primelement == null && this.ultelement == null) {
            this.primelement = teste;
            this.ultelement = teste;
        }else {
            this.ultelement.setProxElemento(teste);
            this.ultelement = teste;
        }

        this.tam++;
    }

    public void removelist(String value) {
        Elemento teste1 = null;
        Elemento teste2 = this.primelement;

        for(int i=0;i<getTam();i++){
            if(teste2.getValor().equalsIgnoreCase(value)){
                teste1.setProxElemento(teste2.getProxElemento());
                teste2=null;
                this.tam--;
                break;
            }
            teste1 = teste2;
            teste2 = teste2.getProxElemento();
        }
    }

    public Elemento getPrimelement() {
        return primelement;
    }

    public void setPrimelement(Elemento primelement) {
        this.primelement = primelement;
    }

    public Elemento getUltelement() {
        return ultelement;
    }

    public void setUltelement(Elemento ultelement) {
        this.ultelement = ultelement;
    }

    public int getTam() {
        return tam;
    }

    public void setTam(int tam) {
        this.tam = tam;
    }

    public void imprime() {
        for(int i =0;i< getTam();i++){
            System.out.println(pegaposicao(i).valor);
        }
    }

}